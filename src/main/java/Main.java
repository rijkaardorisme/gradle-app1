/*
 * Main.java
 * MIT License
 * Rijkaard Orismé <ro.code@yahoo.com>
 * 18/08/2014 11:20 AM
 */

import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main
{
    private static final String filePath = "src/main/resources/users.json";
    
    /**
     * Main method
     * @param args
     * @throws Exception
     * @author Rijkaard Orismé
     */
    public static void main(String[] args) throws Exception
    {
        sayHello("Jenkins");
        handShake();
        sayGoodbye("Jenkins");
    }
    
    /**
     * Say hello and made a short introduction
     * of myself to Jenkins using the data in
     * the JSON file
     * @param person
     * @throws Exception
     */
    public static void sayHello (String person) throws Exception
    {
        try
        {
            FileReader fileReader = new FileReader(filePath);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(fileReader);

            String whoami = new String();
            
            whoami += "My name is " + jsonObject.get("firstname")+ " " + jsonObject.get("lastname");
            whoami += ". I'm a " + jsonObject.get("job") + " at " + jsonObject.get("organization") + ".\nCheers!";

            System.out.println("Hello, " + person + "! " + whoami);
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("FileNotFoundException " + ex.getMessage());
        }
        catch (IOException ex)
        {
            System.out.println("IOException " + ex.getMessage());
        }
        catch (ParseException | NullPointerException ex)
        {
            System.out.println(ex.getClass() + " " + ex.getMessage());
        }
    }
    
    /**
     * Shaking hands with Jenkins
     */
    public static void handShake ()
    {
        System.out.println("Shaking hands...");
    }
    
    /**
     * Saying goodbye to Jenkins!
     * @param person
     */
    public static void sayGoodbye (String person)
    {
        System.out.println("Goodbye, " + person + "!");
    }
}